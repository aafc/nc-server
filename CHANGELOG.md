# Changelog

All notable changes to this project will be added in this file. Changes are released together in a specified build. Tags (for the builds) are created after a new entry in this file is added.

## 1.00.02 - 2020-03-18

### Added

- Small changes for WCAG compliance

## 1.00.01 - 2020-03-02

### Added

- Better error messages for users trying to add files to root directory

### Removed

- Re-added button from build 1.00.00, not necessary after changing quota

## 1.00.00 - 2020-02-24

### Added

- Removed add button for non-admins in the root directory

# Original repo changelog

The change log is at [https://nextcloud.com/changelog/](https://nextcloud.com/changelog/).
