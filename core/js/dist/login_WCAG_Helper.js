//WCAG 4.1.1
//("<title>Login</title>").insertBefore($('html head'))

//WCAG 4.1.1

if ($('#body-login .groupbottom label').length > 0) {

	//insert two line breaks to show password
	$("<br/></br>").insertBefore($('#body-login .groupbottom .submit-wrapper'))

	//underline lost password
	if ($('#lost-password').length > 0) {
		$('#lost-password')[0].style = "text-decoration: underline;";
	}
	//underline Next Cloud on footer
	if ($('#body-login footer p a').length > 0) {
		$('#body-login footer p a')[0].style = "text-decoration: underline;";
	}
	//opens in new window....almost working
	// if($('#body-login footer p').length > 0){
	//     $('#body-login footer p')[0].textContent += " (opens in new window)";
	// }
}

var URL = window.location.href;
console.log(URL);
var n = URL.length;
if ((n > 77) && (window.location.href.indexOf("login?user=") === -1))  {
	window.location.href = URL.substring(0, URL.lastIndexOf("/"));
}
