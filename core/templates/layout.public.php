<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="<?php p($_['language']); ?>" data-locale="<?php p($_['locale']); ?>" >
<head data-requesttoken="<?php p($_['requesttoken']); ?>">
	<meta charset="utf-8">
	<?php
    $title = "Download - Living Laboratories Data Storage Platform";
	$languages = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);

	foreach($languages as $lang)
	{
		if(in_array($lang, "fr") || ($_['language']) === "fr")
		{
			$title = "Download - Plateforme de stockage de données infonuagique des laboratoires vivants";

		}
		else{
			$title = "Download - Living Laboratories Data Storage Platform ";
		}
	}
?>
		
			<title>
			<?php echo $title ?>
		</title>
		
	<title>
	
	</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
	<?php if ($theme->getiTunesAppId() !== '') { ?>
	<meta name="apple-itunes-app" content="app-id=<?php p($theme->getiTunesAppId()); ?>">
	<?php } ?>
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="apple-mobile-web-app-title" content="<?php p((!empty($_['application']) && $_['appid']!=='files')? $_['application']:"Living Laboratories Data Storage Platform"); ?>">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="theme-color" content="<?php p($theme->getColorPrimary()); ?>">
	<link rel="icon" href="<?php print_unescaped(image_path($_['appid'], 'favicon.ico')); /* IE11+ supports png */ ?>">
	<link rel="apple-touch-icon" href="<?php print_unescaped(image_path($_['appid'], 'favicon-touch.png')); ?>">
	<link rel="manifest" href="<?php print_unescaped(image_path($_['appid'], 'manifest.json')); ?>">
	<?php emit_css_loading_tags($_); ?>
	<?php emit_script_loading_tags($_); ?>
	<?php print_unescaped($_['headers']); ?>
</head>
<body id="<?php p($_['bodyid']);?>">
<?php include('layout.noscript.warning.php'); ?>
<?php foreach ($_['initialStates'] as $app => $initialState) { ?>
	<input type="hidden" id="initial-state-<?php p($app); ?>" value="<?php p(base64_encode($initialState)); ?>">
<?php }?>

<?php
if (isset($template)) {
	 $a= ($template->getHeaderTitle()); 
	 if (strpos($a, '.pdf') !== false) {

	
		echo '<div id="warning">';
echo '<p id="forwarning" class="noscript_warning" style="position: inhert;';
echo 'top: 0;';
echo 'width: 100%; /* For Browser Compatibility */';
echo 'background: #AD0000;';
echo 'color: #fff;';
echo 'text-align: center;';
echo 'font-weight: bold;">Please be advised the PDF Viewer is not fully accessible please download and use a different PDF viewer if you are experiencing issues</p>';
echo '</div>';
		}}
?>
<?php
if (isset($template)) {
	$a= ($template->getHeaderTitle()); 
	if (strpos($a, '.pdf') !== false) {
	
	}
	else {
		echo '<a href="#app-content" class="button primary skip-navigation skip-content">';
		p($l->t('Skip to main content'));
		echo '</a>';
	}
}
?>

	<div id="notification-container">
		<div id="notification"></div>
	</div>
	<header id="header">
		<div class="header-left">
			<span id="nextcloud">
				<div class="logo logo-icon svg"></div>
				<h1 class="header-appname">
					<?php if (isset($template)) { p($template->getHeaderTitle()); } else { p($theme->getName());} ?>
				</h1>
				<div class="header-shared-by">
					<?php if (isset($template)) { p($template->getHeaderDetails()); } ?>
				</div>
			</span>
		</div>

		<?php
		/** @var \OCP\AppFramework\Http\Template\PublicTemplateResponse $template */
		if(isset($template) && $template->getActionCount() !== 0) {
			$primary = $template->getPrimaryAction();
			$others = $template->getOtherActions();
			?>
		<div class="header-right">
			<span id="header-primary-action" class="<?php if($template->getActionCount() === 1) {  p($primary->getIcon()); } ?>">
				<a href="<?php p($primary->getLink()); ?>" class="primary button">
					<span><?php p($primary->getLabel()) ?></span>
				</a>
			</span>
			<?php if($template->getActionCount() > 1) { ?>
			<div id="header-secondary-action">
				<button id="header-actions-toggle" class="menutoggle icon-more-white"></button>
				<div id="header-actions-menu" class="popovermenu menu">
					<ul>
						<?php
							/** @var \OCP\AppFramework\Http\Template\IMenuAction $action */
							foreach($others as $action) {
								print_unescaped($action->render());
							}
						?>
					</ul>
				</div>
			</div>
			<?php } ?>
		</div>
		<?php } ?>
	</header>
	<div id="content" class="app-<?php p($_['appid']) ?>" role="main">
		<?php print_unescaped($_['content']); ?>
	</div>
	<?php if(isset($template) && $template->getFooterVisible()) { ?>
	<footer>
		<p><a href="<?php print_unescaped($theme->getBaseUrl()); ?>" target="_blank" rel="noreferrer noopener" class="entity-name"><?php p($theme->getName()); ?> </a><span>  <?php p('The following application is optimized for firefox and Google Chrome'); ?> </span>
		</p>
		<?php

		?>
	</footer>
	<?php } ?>

</body>
</html>
