<!DOCTYPE html>
<html class="ng-csp" data-placeholder-focus="false" lang="<?php p($_['language']); ?>" data-locale="<?php p($_['locale']); ?>" >
	<head
<?php if ($_['user_uid']) { ?>
	data-user="<?php p($_['user_uid']); ?>" data-user-displayname="<?php p($_['user_displayname']); ?>"
<?php } ?>
 data-requesttoken="<?php p($_['requesttoken']); ?>">
		<meta charset="utf-8">
		<?php
    $title = "Login - Living Laboratories Data Storage Platform ";
	$languages = explode(',',$_SERVER['HTTP_ACCEPT_LANGUAGE']);

	foreach($languages as $lang)
	{
		if(in_array($lang, "fr") || ($_['language']) === "fr")
		{
			$title = "Login - Plateforme de stockage de données infonuagique des laboratoires vivants";

		}
		else{
			$title = "Login - Living Laboratories Data Storage Platform ";
		}
	}
?>
		
			<title>
			<?php echo $title ?>
		</title>
		
		
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
		<?php if ($theme->getiTunesAppId() !== '') { ?>
		<meta name="apple-itunes-app" content="app-id=<?php p($theme->getiTunesAppId()); ?>">
		<?php } ?>
		<meta name="theme-color" content="<?php p($theme->getColorPrimary()); ?>">
		<link rel="icon" href="<?php print_unescaped(image_path('', 'favicon.ico')); /* IE11+ supports png */ ?>">
		<link rel="apple-touch-icon" href="<?php print_unescaped(image_path('', 'favicon-touch.png')); ?>">
		<link rel="manifest" href="<?php print_unescaped(image_path('', 'manifest.json')); ?>">
		<?php emit_css_loading_tags($_); ?>
		<?php emit_script_loading_tags($_); ?>
		<?php print_unescaped($_['headers']); ?>
	</head>
	<body id="<?php p($_['bodyid']);?>">
		<?php include 'layout.noscript.warning.php'; ?>
		<?php foreach ($_['initialStates'] as $app => $initialState) { ?>
			<input type="hidden" id="initial-state-<?php p($app); ?>" value="<?php p(base64_encode($initialState)); ?>">
		<?php }?>
		<div class="wrapper">
			<div class="v-align">
				<?php if ($_['bodyid'] === 'body-login' ): ?>
					<header role="banner">
						<div id="header">
							<div class="logo">
								<h1 class="hidden-visually">
								Login -	<?php p($theme->getName()); ?>
								</h1>
								<?php if(\OC::$server->getConfig()->getSystemValue('installed', false)
									&& \OC::$server->getConfig()->getAppValue('theming', 'logoMime', false)): ?>
									<img src="<?php p($theme->getLogo()); ?>"/>
								<?php endif; ?>
								<?php if ($_['bodyid'] === 'password' ): ?>
									<input data-v-2db7cf2a="" id="password" type="password" name="password" autocomplete="on" placeholder="Password" aria-label="Password" required="required" class="password-with-toggle">
									<label for="password">Password</label>
								<?php endif; ?>
							</div>
						</div>
					</header>
				<?php endif; ?>
				<main>
				<div class="panel panel-default login-panel">
      <div class="panel-body" style="padding-top: 30px; padding-bottom: 40px;">
       <div class="row">
        
        <div id="left-box" class="col-md-8 col-xs-12">
         <div class="col-xs-12">
		          </div>        
         <div class="col-xs-12">
          		
					<?php print_unescaped($_['content']); ?>
					</div>
        </div>
        
       </div>
      </div>
     </div>
				</main>
			</div>
		</div>
		<footer role="contentinfo">
			<p class="info">
				<?php print_unescaped($theme->getLongFooter()); ?>
			</p>
		</footer>
	</body>
</html>
