<?php
\OC_Util::addStyle( 'settings', "settings" );
?>
<div id="app-navigation">
	<ul>
	<?php if($_['admin']) { ?>
		
	<?php } ?>

		<li>
			<a href="https://docs.nextcloud.com" class="icon-category-office" target="_blank" rel="noreferrer noopener">
				<?php p($l->t('Documentation')); ?> ↗
			</a>
		</li>
		<li>
			<a href="https://help.nextcloud.com" class="icon-comment" target="_blank" rel="noreferrer noopener">
				<?php p($l->t('Forum')); ?> ↗
			</a>
		</li>
</div>

<div id="app-content" class="help-includes">
	
</div>
